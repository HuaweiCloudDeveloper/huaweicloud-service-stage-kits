## 仓库简介

ServiceStage是面向企业的应用管理与运维平台，提供应用开发、构建、发布、监控及运维等一站式解决方案。提供Java、Go、PHP、Node.js、Docker、Tomcat等运行环境，支持微服务应用、Web应用以及通用应用的托管与治理，让企业应用上云更简单。

#  项目总览

<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>
     <tr>
        <td rowspan="1">漏洞扫描</td>
        <td>基于Jenkins构建任务调用VSS服务API对WEB网站进行漏洞扫描的实践</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-vss-api-jenkins-intergration-sample">huaweicloud-VSS-API-jenkins-intergration-sample</a></td>  
    <tr>
        <td rowspan="1">网站安全</td>
        <td>基于华为云Web应用防火墙WAF构建的解决方案</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-website-security">huaweicloud-solution-website-security</a></td>
    </tr> 
    <tr>
        <td rowspan="1">无服务告警推送</td>
        <td>快速构建一个一键自动部署的无服务器告警推送解决方案
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-alert-notifier">huaweicloud-solution-serverless-alert-notifier</a></td>
    </tr> 
     <tr>
        <td rowspan="1">日志运维分析</td>
        <td>基于华为云ELB（弹性负载均衡）日志运维分析解决方案</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-website-om-based-on-log-tank-service">huaweicloud-solution-website-om-based-on-log-tank-service</a></td>
    </tr> 
     <tr>
        <td rowspan="1">开源项目</td>
        <td>基于springcloud微服务架构并结合华为云能力改造的开源项目
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-servicestage-cse-cce-saas-housekeeper">huaweicloud-servicestage-cse-cce-saas-housekeeper</a></td>
    </tr> 
     <tr>
        <td rowspan="1">ServiceStage</td>
        <td>提供应用开发、构建、发布、监控及运维等一站式解决方案
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-servicestage-components-version-upgrade-plugins">huaweicloud-servicestage-components-version-upgrade-plugins</a></td>
    </tr> 
     <tr>
        <td rowspan="1">LTS</td>
        <td>华为云日志服务LTS提供一站式日志采集、秒级搜索、海量存储等功能
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-lts-sdk-intergration-sample">huaweicloud-LTS-SDK-intergration-sample</a></td>
    </tr> 
    <tr>
        <td rowspan="3">CloudBuild</td>
        <td>通过Job ID查询30天内构建任务历史列表
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-build-show-list-period-history-java">huaweicloud-CloudBuild-ShowListPeriodHistory-java</a></td>
    </tr>
    <tr>
    <td>通过Project ID查询项目下编译构建任务列表
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-build-show-job-list-java">huaweicloud-CloudBuild-ShowJobList-java</a></td>
    </tr> 
    <tr>
<td>通过Job ID启动执行构建任务
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloud-build-run-job-java">huaweicloud-CloudBuild-RunJob-java</a></td>
    </tr> 
<tr>
        <td rowspan="3">KooMessage服务</td>
        <td>智能信息基础版的发送
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-vms-java">huaweicloud-vms-java</a></td>
    </tr>
    <tr>
    <td>查询智能信息基础版模板状态列表
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-aim-java">huaweicloud-aim-java</a></td>
    </tr> 
    <tr>
		<td>提供信息注册通道用于发送智能消息
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sa-java">huaweicloud-sa-java
</a></td>
    </tr>
	<tr>
        <td rowspan="2">CAE</td>
        <td>前端代码
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-frontend"> huaweicloud-cae-frontend
</a></td>
    </tr> 
    <td>后端代码
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-backend">huaweicloud-cae-backend</a></td>
    </tr>
	<tr>
        <td rowspan="1">示例Demo</td>
        <td>该代码仓用于开源华为云apm开发的示例Demo
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-apm-java">huaweicloud-apm-java</a></td>
    </tr> 
<tr>
        <td rowspan="5">MetaSpace应用服务组件</td>
        <td>游戏服务器弹性扩缩容
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-metaspace-aass">huaweicloud-solution-metaspace-aass</a></td>
    </tr> 
	<tr>
    <td>分配服务端会话资源
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-metaspace-appgateway">huaweicloud-solution-metaspace-appgateway</a></td>
    </tr> 
	<tr>
		<td>提供信息注册通道用于发送智能消息
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-sa-java">huaweicloud-solution-metaspace-auxproxy
</a></td>
    </tr>
	<tr>
		<td>负责应用进程的全局化动态部署及管理
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-metaspace-fleetmanager">huaweicloud-solution-metaspace-fleetmanager
</a></td>
    </tr>
	<tr>
		<td>帮助开发者快速构建稳定、低延时的服务端应用，兼容所有gRPC支持的服务框架的部署与运行
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-metaspace">huaweicloud-solution-metaspace
</a></td>
    </tr>
    <tr>
        <td rowspan="3">应用性能管理</td>
        <td>用户查看已有配置好的URL跟踪信息
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-url-tracking-java">huaweicloud-url-tracking-java（待发布）</a></td>
    </tr> 
	<tr>
    	<td>用户查看已有应用下告警列表及消息
		</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-alarm-list-java">huaweicloud-alarm-list-java（待发布）</a></td>
    </tr> 
	<tr>
		<td>用户查看已有应用的拓扑图
		</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-topology-java">huaweicloud-topology-java（待发布）</a></td>
    </tr> 
	<tr>
        <td rowspan="1">Kafka</td>
        <td>基于SaaS项目的技术实践
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-kafka-sample-java">huaweicloud-kafka-sample-java</a></td>
    </tr> 
    <tr>
        <td rowspan="3">管理CloudArtifact制品仓库服务</td>
        <td>管理CloudArtifact制品仓库服务的API场景示例和服务场景示例代码（java）
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-url-tracking-java">huaweicloud-cloudartifact-java</a></td>
    </tr> 
	<tr>
    <td>管理CloudArtifact制品仓库服务的API场景示例和服务场景示例代码（Python）
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudartifact-python">huaweicloud-cloudartifact-python</a></td>
    </tr> 
	<tr>
<td>管理CloudArtifact制品仓库服务的API场景示例和服务场景示例代码（Go）
	</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cloudartifact-go">huaweicloud-cloudartifact-go</a></td>
    </tr> 
            </td>
</tr>
</table>






​        







